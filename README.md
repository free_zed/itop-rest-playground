iTop REST/JSON API Playground
=============================

Code from [iTop Hub][itophub], _JS Fiddle_ is also [avaiable][jsf].

[itophub]:  https://www.itophub.io/wiki/page?id=2_6_0%3Aadvancedtopics%3Arest_json_playground
[jsf]:      https://jsfiddle.net/bcfh4zm7/
