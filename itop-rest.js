/*
    Author: Pierre Goiffon
    Source: https://jsfiddle.net/pgoiffon/8fs67agL/
    Licence: AGPL - https://www.itophub.io/page/terms-of-use
*/
function DoXSS(oJSON)
{
	var sVersion = $('#version').val();
	var sURL = $('#server_url').val()+"/webservices/rest.php?version="+sVersion;
	$('#result').html('');
	var sDataType = 'json';
	if ($(':input[name=json]:checked').val() == 'jsonp')
	{
		sDataType = 'jsonp';
	}

    $.ajax({
        type: "POST",
        url: sURL,
        dataType: sDataType,
		data: { auth_user: $('#auth_user').val(), auth_pwd: $('#auth_pwd').val(), json_data: JSON.stringify(oJSON) },
		crossDomain: 'true',
        success: function (data) {
			$('#result').html(syntaxHighlight(data));
        }
    });
	return false;
}

function ListOperations()
{
	$('#result').val('');
	var oJSON = {
		operation: 'list_operations'
	};

	DoXSS(oJSON);
}

function GetObject()
{
	$('#result').val('');
	var oJSON = {
		operation: 'core/get',
		'class': $('#get_class').val(),
		key: $('#get_key').val()
	};

	DoXSS(oJSON);
}

function syntaxHighlight(json) {
    if (typeof json != 'string') {
         json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}
